//
//  RTSwizzlingAndSubstitutionMethods.m
//  Total
//
//  Created by Алексей Борисов on 14.08.14.
//  Copyright (c) 2014 Алексей Борисов. All rights reserved.
//

#import "RTSwizzlingAndSubstitutionMethods.h"

@interface RTSwizzlingAndSubstitutionMethods ()
{
    NSMutableArray *_substitutionLogMessagesStorage;
    IMP oldImplementation;
}

@property (weak, nonatomic) IBOutlet UITextView *substitutionLogTextView;

@end

@implementation RTSwizzlingAndSubstitutionMethods


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.swizzlingLogMessageStorage = [NSMutableArray arrayWithCapacity:10];
    _substitutionLogMessagesStorage = [NSMutableArray arrayWithCapacity:10];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark - Logic

-(void)methodOne
{
    [Helper addLogMessage:@"Это первый метод!"
                toStorage:_substitutionLogMessagesStorage
             fromTextView:_substitutionLogTextView];
}

-(void)methodTwo
{
    [Helper addLogMessage:@"Это второй метод!"
                toStorage:_substitutionLogMessagesStorage
             fromTextView:_substitutionLogTextView];
}

-(void)originalMethod
{
    [Helper addLogMessage:@"Это пример оригинального метода"
                toStorage:_swizzlingLogMessageStorage
             fromTextView:_swizzlingLogTextView];
}

-(void)methodFromSwizzling
{
    [Helper addLogMessage:@"Это пример оригинального метода"
                toStorage:self.swizzlingLogMessageStorage
             fromTextView:self.swizzlingLogTextView];
}

#pragma mark -
#pragma mark - Buttons handler

- (IBAction)clearLog:(UIButton *)sender {
    switch (sender.tag) {
        case 121:{
            [_substitutionLogMessagesStorage removeAllObjects];
            [Helper drawLogInTextView:_substitutionLogTextView
                          fromStorage:_substitutionLogMessagesStorage];
            break;
        }
        case 122:{
            [self.swizzlingLogMessageStorage removeAllObjects];
            [Helper drawLogInTextView:self.swizzlingLogTextView
                          fromStorage:self.swizzlingLogMessageStorage];
        }
        default:
            break;
    }
}

- (IBAction)actionFromMethodOne:(id)sender {
    [self methodOne];
}

- (IBAction)actionFromMethodTwo:(id)sender {
    [self methodTwo];
}

- (IBAction)substitutionAction:(id)sender {
    IMP methodImp = class_getMethodImplementation([self class], @selector(methodOne));
    Method new = class_getInstanceMethod([self class], @selector(methodTwo));
    IMP currentImplementation = method_getImplementation(new);
    if (!oldImplementation){
        oldImplementation = currentImplementation;
    }
    if (currentImplementation != oldImplementation) {
        method_setImplementation(new, oldImplementation);
        [Helper addLogMessage:@"Возвращена старая имплементация."
                    toStorage:_substitutionLogMessagesStorage
                 fromTextView:_substitutionLogTextView];
    } else {
        method_setImplementation(new, methodImp);
        [Helper addLogMessage:@"Установлена новая имплементация."
                    toStorage:_substitutionLogMessagesStorage
                 fromTextView:_substitutionLogTextView];
    }
}

- (IBAction)actionFromOriginalMethod:(id)sender {
    [self originalMethod];
}

- (IBAction)swizzlingAction:(id)sender {
    [self methodFromSwizzling];
}

@end
