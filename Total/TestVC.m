//
//  TestVC.m
//  Total
//
//  Created by Алексей Борисов on 15.08.14.
//  Copyright (c) 2014 Алексей Борисов. All rights reserved.
//

#import "TestVC.h"

@interface TestVC ()

@end

@implementation TestVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIView *greenView = [[UIView alloc]init];
    [greenView setBackgroundColor:[UIColor greenColor]];
    [greenView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.view addSubview:greenView];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:greenView
                                                          attribute:NSLayoutAttributeTop
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeTop
                                                         multiplier:1.f
                                                           constant:self.navigationController.navigationBar.frame.size.height + 20]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:greenView
                                                          attribute:NSLayoutAttributeLeft
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeLeft
                                                         multiplier:1.f
                                                           constant:10]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:greenView
                                                          attribute:NSLayoutAttributeWidth
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:nil
                                                          attribute:NSLayoutAttributeNotAnAttribute
                                                         multiplier:1.f
                                                           constant:150]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:greenView
                                                          attribute:NSLayoutAttributeBottom
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeBottom
                                                         multiplier:1.f
                                                           constant:-50.f]];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)testBtn:(id)sender {

}

@end
