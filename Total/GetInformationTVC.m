//
//  GetInformationTVC.m
//  Total
//
//  Created by Алексей Борисов on 15.08.14.
//  Copyright (c) 2014 Алексей Борисов. All rights reserved.
//

#import "GetInformationTVC.h"

@interface GetInformationTVC ()
{
    NSMutableArray *_logMessagesStorage;
}

@property (weak, nonatomic) IBOutlet UITextView *logTextView;

@end

@implementation GetInformationTVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _logMessagesStorage = [NSMutableArray arrayWithCapacity:10];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - 
#pragma mark - Logic

-(NSString*)getDescription
{
    NSMutableDictionary *propertyValues = [NSMutableDictionary dictionary];
    unsigned int propertyCount;
    objc_property_t *properties = class_copyPropertyList([self class], &propertyCount);
    for (unsigned int i = 0; i<propertyCount; i++) {
        char const *propertyName = property_getName(properties[i]);
        char const *attr = property_getAttributes(properties[i]);
        if (attr[1]== '@') {
            NSString *selector = [NSString stringWithCString:propertyName encoding:NSUTF8StringEncoding];
            SEL sel = sel_registerName([selector UTF8String]);
            NSObject *propertyValue = objc_msgSend(self, sel);
            propertyValues[selector] = propertyValue.description;
        }
    }
    free(properties);
    
    return [NSString stringWithFormat:@"%@",propertyValues];
}

#pragma mark -
#pragma mark - Log logic

-(void)addMeesageToLog:(NSString *)message
{
    [Helper addLogMessage:message toStorage:_logMessagesStorage fromTextView:_logTextView];
}

#pragma mark -
#pragma mark - Buttons handler

- (IBAction)clearLog:(id)sender
{
    [_logMessagesStorage removeAllObjects];
    [Helper drawLogInTextView:_logTextView fromStorage:_logMessagesStorage];
}

- (IBAction)getAndPrintDescriptionAction:(id)sender
{
    [self addMeesageToLog:[NSString stringWithFormat:@"Имя класса, полученное через NSStringFromClass:\n %@ \n",NSStringFromClass([self class])]];
    [self addMeesageToLog:[NSString stringWithFormat:@"Имя класса, полученное через class_getName:\n %s \n",class_getName([self class])]];
    [self addMeesageToLog:[NSString stringWithFormat:@"Суперкласс нашего класса:\n %@ \n",class_getSuperclass([self class])]];
    [self addMeesageToLog:[NSString stringWithFormat:@"Описание свойств класса: \n %@ \n",[self getDescription]]];
    [self addMeesageToLog:[NSString stringWithFormat:@"Название метода, составляющего описание (получен через NSStringFromSelector): \n %@ \n",NSStringFromSelector(@selector(getDescription))]];
}

@end
