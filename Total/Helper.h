//
//  Helper.h
//  Total
//
//  Created by Алексей Борисов on 14.08.14.
//  Copyright (c) 2014 Алексей Борисов. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Helper : NSObject

+(void)addLogMessage:(NSString *)message toStorage:(NSMutableArray *)storage fromTextView:(UITextView *)textView;
+(void)drawLogInTextView:(UITextView *)textView  fromStorage:(NSMutableArray *)storage;

-(NSString *)stolenMethodWithIncomingRowNumber:(int)rowNumber;
@end
