//
//  DMResolution.m
//  Total
//
//  Created by Алексей Борисов on 18.08.14.
//  Copyright (c) 2014 Алексей Борисов. All rights reserved.
//

#import "DMResolution.h"

@implementation DMResolution

+ (BOOL)resolveInstanceMethod:(SEL)sel
{
    class_addMethod([self class], sel, (IMP)genericMethodIMP, "@@:");
    return YES;
}

NSString * genericMethodIMP(id self, SEL _cmd)
{
    return [NSString stringWithFormat:@"Привет из метода c именем \"%@\", созданного динамически",NSStringFromSelector(_cmd)];
}

@end
