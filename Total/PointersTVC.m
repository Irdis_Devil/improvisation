//
//  PointersTVC.m
//  Total
//
//  Created by Алексей Борисов on 14.08.14.
//  Copyright (c) 2014 Алексей Борисов. All rights reserved.
//

#import "PointersTVC.h"

#define SET_VALUE_FROM_LABEL(value,label)  [self setValue:value fromLabel:label]

@interface PointersTVC (){
    
    int firstLabelValue,
    secondLabelValue,
    thirdLabelValue;
    
    NSMutableArray *_logMessagesStorage;
}

@property (weak, nonatomic) IBOutlet UILabel *firstLabel;
@property (weak, nonatomic) IBOutlet UILabel *secondLabel;
@property (weak, nonatomic) IBOutlet UILabel *thirdLabel;
@property (weak, nonatomic) IBOutlet UITextView *logTextView;

@end

@implementation PointersTVC


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _logMessagesStorage = [NSMutableArray arrayWithCapacity:10];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self clearValuesAndReDrawLabels];
}

#pragma mark -
#pragma mark - Logic

-(void)setValue:(int)value fromLabel:(UILabel*)label
{
    [label setText:[NSString stringWithFormat:@"%i",value]];
}

-(void)clearValuesAndReDrawLabels
{
    firstLabelValue = 0;
    secondLabelValue = 0;
    thirdLabelValue = 0;
    [self drawZeroValues];
}

-(void)drawZeroValues
{
    SET_VALUE_FROM_LABEL(firstLabelValue, _firstLabel);
    SET_VALUE_FROM_LABEL(secondLabelValue, _secondLabel);
    SET_VALUE_FROM_LABEL(thirdLabelValue, _thirdLabel);
}

-(void)increment:(int *)value
{
    *value = *value + 1;
}

#pragma mark -
#pragma mark - Log logic

-(void)addMeesageToLog:(NSString *)message
{
    [Helper addLogMessage:message toStorage:_logMessagesStorage fromTextView:_logTextView];
}

#pragma mark -
#pragma mark - Buttons handler

-(IBAction)incrementButtonWasPressed:(UIButton *)sender
{
    [self addMeesageToLog:[NSString stringWithFormat:@"Нажата кнопка с тэгом: %i",sender.tag]];
    switch (sender.tag) {
    case 111: {
        [self increment:&firstLabelValue];
        SET_VALUE_FROM_LABEL(firstLabelValue, _firstLabel);
        break;
        }
    case 112: {
        [self increment:&secondLabelValue];
        SET_VALUE_FROM_LABEL(secondLabelValue, _secondLabel);
        break;
        }
    case 113: {
        [self increment:&thirdLabelValue];
        SET_VALUE_FROM_LABEL(thirdLabelValue, _thirdLabel);
        break;
        }
    }
}

- (IBAction)clearLog:(id)sender
{
    [_logMessagesStorage removeAllObjects];
    [Helper drawLogInTextView:_logTextView fromStorage:_logMessagesStorage];
}

- (IBAction)clearValues:(id)sender
{
    [self clearValuesAndReDrawLabels];
    [self addMeesageToLog:@"Были сброшены все значения."];
}

@end
