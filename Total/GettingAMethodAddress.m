//
//  GettingAMethodAddress.m
//  Total
//
//  Created by Алексей Борисов on 15.08.14.
//  Copyright (c) 2014 Алексей Борисов. All rights reserved.
//

#import "GettingAMethodAddress.h"

@interface GettingAMethodAddress ()
{
    NSMutableArray *_logMessagesStorage;
}

@property (weak, nonatomic) IBOutlet UITextView *logTextView;

@end

@implementation GettingAMethodAddress

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _logMessagesStorage = [NSMutableArray arrayWithCapacity:10];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark -
#pragma mark - Buttons handler

- (IBAction)clearLog:(id)sender
{
    [_logMessagesStorage removeAllObjects];
    [Helper drawLogInTextView:_logTextView fromStorage:_logMessagesStorage];
}

- (IBAction)stoleMethodAction:(id)sender
{
    NSString * (*stole)(id, SEL, int);
    Helper *hlp = [Helper new];
    stole = (NSString *(*)(id, SEL, int))[hlp methodForSelector:@selector(stolenMethodWithIncomingRowNumber:)];
    for (int i = 0 ; i < 5; i++) {
        [Helper addLogMessage:stole([hlp class], @selector(stolenMethodWithIncomingRowNumber:), i)
                    toStorage:_logMessagesStorage
                 fromTextView:_logTextView];
    }
}

@end
