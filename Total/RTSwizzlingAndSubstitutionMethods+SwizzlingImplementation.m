//
//  RTSwizzlingAndSubstitutionMethods+SwizzlingImplementation.m
//  Total
//
//  Created by Алексей Борисов on 14.08.14.
//  Copyright (c) 2014 Алексей Борисов. All rights reserved.
//

#import "RTSwizzlingAndSubstitutionMethods+SwizzlingImplementation.h"

@implementation RTSwizzlingAndSubstitutionMethods (SwizzlingImplementation)

//Мы перегружаем метод load — это специальный callback, который, если он определен в классе, будет вызван во время инициализации этого класса — до вызова любого из других его методов
+(void)load
{
    Method oldMethod = class_getInstanceMethod([self class], @selector(methodFromSwizzling));
    Method newMethod = class_getInstanceMethod([self class], @selector(newMethodImplementation));
    method_exchangeImplementations(oldMethod, newMethod);
}

-(void)newMethodImplementation
{
    [Helper addLogMessage:@"Код выполнен из засвиззленного метода"
                toStorage:self.swizzlingLogMessageStorage
             fromTextView:self.swizzlingLogTextView];
}


@end
