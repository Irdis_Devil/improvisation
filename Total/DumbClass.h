//
//  DumbClass.h
//  Total
//
//  Created by Алексей Борисов on 18.08.14.
//  Copyright (c) 2014 Алексей Борисов. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DMResolution.h"

@protocol MessagingWithDumbClass <NSObject>

@required
-(void)reciveMessageFromDubClass:(NSString *)message;

@end

@interface DumbClass : NSObject

@property (weak, nonatomic) id <MessagingWithDumbClass> delegate;

@end
