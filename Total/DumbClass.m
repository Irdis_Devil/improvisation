//
//  DumbClass.m
//  Total
//
//  Created by Алексей Борисов on 18.08.14.
//  Copyright (c) 2014 Алексей Борисов. All rights reserved.
//

#import "DumbClass.h"
#import "DMResolution.h"

@implementation DumbClass

-(NSMethodSignature *)methodSignatureForSelector:(SEL)aSelector
{
    [self sendMessage:@"Селектор не найден. Запрашивается сигнатура метода."];
    
    NSMethodSignature *methodSignature = [[NSMethodSignature alloc] init];
    @try {
        [self sendMessage:@"Получение сигнатуры динамического метода"];
        methodSignature = [[DMResolution class] instanceMethodSignatureForSelector:aSelector];
    }
    @catch (NSException *exception) {
        UIAlertView *errorHandler = [[UIAlertView alloc] initWithTitle:@"Ошибка"
                                                               message:exception.reason
                                                              delegate:nil
                                                     cancelButtonTitle:@"Ok"
                                                     otherButtonTitles:nil];
        [errorHandler show];
        [self sendMessage:@"Произошла ошибка"];
    }
    @finally {
        if (methodSignature){
            [self sendMessage:@"Meтод успешно создан, сигнатура получена"];
        }
    }
    return methodSignature;
}

-(void)forwardInvocation:(NSInvocation *)anInvocation
{
    if ([anInvocation methodSignature]){
        [self sendMessage:@"Начинается перенаправление вызова"];
        DMResolution *resolution = [DMResolution new];
        __unsafe_unretained NSString *result = nil;
        if ([resolution respondsToSelector:anInvocation.selector]) {
            [anInvocation invokeWithTarget:resolution];
            [anInvocation getReturnValue:&result];
            [self sendMessage:result];
        } else {
            [super forwardInvocation:anInvocation];
        }
    } else {
        UIAlertView *errorOther = [[UIAlertView alloc] initWithTitle:@"Ошибка"
                                                             message:@"Что-то пошло не так и сигнатура пуста."
                                                            delegate:nil
                                                   cancelButtonTitle:@"Ok"
                                                   otherButtonTitles:nil];
        [errorOther show];
        [self sendMessage:@"Произошла ошибка"];
    }
}

-(void)sendMessage:(NSString *)message
{
    if ([self.delegate respondsToSelector:@selector(reciveMessageFromDubClass:)]){
        [self.delegate performSelector:@selector(reciveMessageFromDubClass:) withObject:message];
    }
}

-(void)test
{
    [self sendMessage:@"Оп-пачки, а метод-то нашелся"];
}

@end
