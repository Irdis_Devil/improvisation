//
//  RTSwizzlingAndSubstitutionMethods.h
//  Total
//
//  Created by Алексей Борисов on 14.08.14.
//  Copyright (c) 2014 Алексей Борисов. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RTSwizzlingAndSubstitutionMethods : UITableViewController

@property (strong, nonatomic) NSMutableArray *swizzlingLogMessageStorage;
@property (weak, nonatomic) IBOutlet UITextView *swizzlingLogTextView;

-(void)methodFromSwizzling;

@end
