//
//  DynamicMethodResolutionTVC.m
//  Total
//
//  Created by Алексей Борисов on 18.08.14.
//  Copyright (c) 2014 Алексей Борисов. All rights reserved.
//

#import "DynamicMethodResolutionTVC.h"
#import "DumbClass.h"

@interface DynamicMethodResolutionTVC () <UITextFieldDelegate, MessagingWithDumbClass>
{
    NSMutableArray *_logMessagesStorage;
}

@property (weak, nonatomic) IBOutlet UITextField *enterMethodNameTextField;
@property (weak, nonatomic) IBOutlet UITextView *logTextView;

@end

@implementation DynamicMethodResolutionTVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _logMessagesStorage = [NSMutableArray arrayWithCapacity:10];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark - Log logic

-(void)addMeesageToLog:(NSString *)message
{
    [Helper addLogMessage:message toStorage:_logMessagesStorage fromTextView:_logTextView];
}

#pragma mark -
#pragma mark - Buttons handler

- (IBAction)clearLog:(id)sender
{
    [_logMessagesStorage removeAllObjects];
    [Helper drawLogInTextView:_logTextView fromStorage:_logMessagesStorage];
}

- (IBAction)createButtonWasPressed:(id)sender {
    [self.enterMethodNameTextField endEditing:YES];
    if (self.enterMethodNameTextField.text.length >0){
        [self addMeesageToLog:[NSString stringWithFormat:@"Вы ввели: %@", self.enterMethodNameTextField.text]];
        SEL newMethodSelector = NSSelectorFromString(self.enterMethodNameTextField.text);
        [self addMeesageToLog:[NSString stringWithFormat:@"Создан селектор с именем: %@",NSStringFromSelector(newMethodSelector)]];
        DumbClass *dc = [DumbClass new];
        dc.delegate = self;
        @try {
            [dc performSelector:newMethodSelector];
        }
        @catch (NSException *exception) {
            UIAlertView *errorHandler = [[UIAlertView alloc] initWithTitle:@"Ошибка"
                                                                   message:exception.reason
                                                                  delegate:nil
                                                         cancelButtonTitle:@"Ok"
                                                         otherButtonTitles:nil];
            [errorHandler show];
            [self addMeesageToLog:@"Произошла ошибка"];
        }
        @finally {
            self.enterMethodNameTextField.text = nil;
        }
        
    } else {
        UIAlertView *emptyAlert = [[UIAlertView alloc] initWithTitle:@"Ошибка"
                                                             message:@"Название не может быть пустым"
                                                            delegate:nil
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles: nil];
        [emptyAlert show];
    }
    
}

#pragma mark -
#pragma mark - Dumb Class Delegate

-(void)reciveMessageFromDubClass:(NSString *)message
{
    [self addMeesageToLog:message];
}

#pragma mark - 
#pragma  mark -Text Field Delegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    BOOL _isAllowed = YES;
    NSString *nameRegex = @"[A-Za-z]+";
    NSPredicate *nameTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", nameRegex];
    
    if (range.length>0){
        return YES;
        
    }
    
    if (![nameTest evaluateWithObject:string])
    {
        [self.enterMethodNameTextField endEditing:YES];
        UIAlertView *userIsDumb = [[UIAlertView alloc] initWithTitle:@"Ошибка"
                                                             message:@"Уважаемый, научитесь читать: ТОЛЬКО БУКВЫ ЛАТИНСКОГО АЛФАВИТА!"
                                                            delegate:nil
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles: nil];
        [userIsDumb show];

        _isAllowed =  NO;
    }
    
    return _isAllowed;
}

@end
