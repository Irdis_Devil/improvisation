//
//  Helper.m
//  Total
//
//  Created by Алексей Борисов on 14.08.14.
//  Copyright (c) 2014 Алексей Борисов. All rights reserved.
//

#import "Helper.h"

@implementation Helper

#pragma mark -
#pragma mark - Log logic

+(void)addLogMessage:(NSString *)message toStorage:(NSMutableArray *)storage fromTextView:(UITextView *)textView
{
    if (storage.count == 10){
        [storage removeObjectAtIndex:0];
    }
    [storage addObject:message];
    [self drawLogInTextView:textView fromStorage:storage];
}

+(void)drawLogInTextView:(UITextView *)textView  fromStorage:(NSMutableArray *)storage
{
    NSString *logText = [NSString string];
    for (NSString *message in storage) {
        logText = [logText stringByAppendingString:[NSString stringWithFormat:@"%@\n",message]];
    }
    textView.text = logText;
}

-(NSString *)stolenMethodWithIncomingRowNumber:(int)rowNumber
{
    return [NSString stringWithFormat:@"%i. Украли!",rowNumber];
}

@end
